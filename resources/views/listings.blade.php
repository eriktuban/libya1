<h1>{{$heading}}</h1>
@unless(count((array)$listings) == 0)
@foreach($listings as $listing)
<h2> 
    <a href="/listing/{{$listing['id']}}">{{$listing['title']}}
    </a>
</h2>
<p>Venue:  {{$listing['location']}} </p>
<p>Event Date:  {{$listing['eventdate']}} | Entrance Fee: {{$listing['fee']}}</p>
<p>Event detail: {{$listing['details']}}</p>
<hr />
@endforeach

@else
<p>No listings found</p>
@endunless
