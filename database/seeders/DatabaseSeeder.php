<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use App\Models\Listing;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(7)->create();
   
        Listing::create([
                'title' => 'Secret Funs',
                'tags' => 'indie rock, folk',
                'production' => 'Secret Sauce',
                'location' => 'Laurencias Grill',
                'fee' => '200 with FREE BEER',
                'eventdate' => '2022-09-26',                
                'email' => 'no@name.com',
                'link' => 'http://test.com',
                'details' => 'Featuring Folk Hogan, Highway Seven Circus, etc.'
            ]);
      Listing::create([
                'title' => 'TUIS',
                'tags' => 'indie rock, post-punk, protest rock',
                'production' => 'Dakila, PAWN, Counterflow Productions',
                'location' => 'Laurencias Grill',
                'fee' => '100 with FREE BEER',
                'eventdate' => '2022-11-26',
                'email' => 'no@name.com',
                'link' => 'http://test.com',
                'details' => 'Featuring \n KANA, The Spirals, The Pervs, etc'
            ]); 

    //     \App\Models\User::factory()->create([
     //        'name' => 'Test User',
      //       'email' => 'test@example.com',
      //   ]);
    }
}
